/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derStricher;

/**
 *
 * @author valentinpaal
 */
public class User {
    private String name;
    private String secret;
    private boolean admin;

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    

    public User(String name, String password, boolean admin) {
        this.name = name;
        this.admin = admin;
        this.secret = Authentificator.hash(password);
    }
    
    public User(String name, String password) {
        this.name = name;
        this.admin = false;
        this.secret = Authentificator.hash(password);
    }

   
    public void setPassword(String password) {
        this.secret = Authentificator.hash(password);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecret() {
        return secret;
    }
    
    public void deleteUser() {
        Authentificator.delUser(this);
    }
    
    public String serialise(){
        return name +"&"+secret +"&"+ admin;
    }
    
    public static User deSerialize(String string){
        String strings[] = string.split("&");
        User user = new User(strings[0], strings[1], Boolean.valueOf(strings[2]));
        user.secret = strings[1];
        return user;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derStricher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author valentinpaal
 */
public class Persistence {
    public static void saveAzubiToFile(String filename, ArrayList<Azubi> azubis) throws FileNotFoundException {
        String fileName = filename;
        PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));
        azubis.forEach((azubi) -> {
            pw.println(azubi.serialise());
        });
        pw.close();
        System.err.println("Saved");
    }
    
    public static ArrayList<Azubi> loadAzubiFromFile(String filename) throws FileNotFoundException, IOException {
        String fileName = filename;
        ArrayList<Azubi> azubis = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            while (br.ready()){
            String line = br.readLine();
            Azubi azubi = Azubi.deSerialize(line);
            azubis.add(azubi);
            }
        return azubis;
    }
    
    public static void saveUserToFile(String filename, ArrayList<User> users) throws FileNotFoundException {
        String fileName = filename;
        PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));
        users.forEach((user) -> {
            pw.println(user.serialise());
        });
        pw.close();
    }
    
    public static ArrayList<User> loadUserFromFile(String filename) throws FileNotFoundException, IOException {
        String fileName = filename;
        ArrayList<User> users = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            while (br.ready()){
            String line = br.readLine();
            User user = User.deSerialize(line);
            users.add(user);
            }
        return users;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derStricher;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author valentinpaal
 */
@ApplicationScoped
@Named("azubi")
public class Azubi {

    private final String name;
    private int red;
    private int blue;

    public Azubi(String name) {
        this.name = name;
    }
    
    public Azubi() {
        this.name = "name";
    }
    
    public Azubi(String name, int blue, int red) {
        this.name = name;
        this.blue = blue;
        this.red = red;
    }

    public boolean equals(Azubi azubi){
        return azubi.name == null ? this.name == null : azubi.name.equals(this.name);
    }
    
    public String helloWorld() {
        return "Hi";
    }

    public String getRed() {
        String strR = "";
        for (int i = 0; i < red; i++) {
            strR = strR + "<div class=\"stroke\"></div>";
        }

        return strR;
    }

    public String getBlue() {
        String strR = "";
        for (int i = 0; i < blue; i++) {
            strR = strR + "<div class=\"stroke\"></div>";
        }

        return strR;
    }

    public void increaseRed() {

        red++;

    }

    public void increaseBlue() {

        blue++;

    }

    public void decreaseRed() {

        if (red > 0) {
            red--;
        }

    }

    public void decreaseBlue() {

        if (blue > 0) {
            blue--;
        }

    }

    public String getName() {
        return name;
    }

    public int getSum() {
        return blue - 2 * red;
    }
    
    public String serialise(){
        return name +"&"+blue +"&"+ red;
    }
    
    public static Azubi deSerialize(String string){
        String strings[] = string.split("&");
        Azubi azubi = new Azubi(strings[0], Integer.valueOf(strings[1]), Integer.valueOf(strings[2]));
        return azubi;
    }

}

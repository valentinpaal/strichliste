/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derStricher;

import java.io.Serializable;
import java.util.Random;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author valentinpaal
 */
@SessionScoped
@Named("navi")
public class Navigator implements Serializable{
    
    private final Random random = new Random();
    
    public String getStart(){
        return "index.xhtml";
    }
    
    public String getUser(){
        return "user.xhtml";
    }
    
    public String getLogin(){
        if (random.nextInt() % 50 == 42){
            return "normal.xhtml";
            
        } else{
             return "settings.xhtml";
        }
       
    }
    
    public String getHistory(){
        return "history.xhtml";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derStricher;

import java.util.ArrayList;

/**
 *
 * @author valentinpaal
 */
public class Monat {

    private String date;
    private final ArrayList<Azubi> AZUBIS;

    public ArrayList<Azubi> getAZUBIS() {
        return AZUBIS;
    }

    public Monat(String date, ArrayList<Azubi> azubis) {
        this.date = date;
        AZUBIS = azubis;
    }

    public String getCalendar() {
        return date;
    }

    public void setCalendar(String date) {
        this.date = date;
    }

    public String getDate() {
        //Month.of(monat).getDisplayName(TextStyle.FULL, Locale.GERMAN)
        return date;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derStricher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author valentinpaal
 */
@SessionScoped
@Named("auth")
public class Authentificator implements Serializable {

    private boolean auth;
    private User activeAccount;
    private static ArrayList<User> accounts = new ArrayList();
    private String name;
    private String password;
    private String status;
    private String welcome = "Wilkommen Unwürdiger";
    private final String path = "UserList";

    public String getWelcome() {
        return welcome;
    }

    public String getName() {
        return name;
    }

    public Authentificator() {
        loadUser();

        if(getUserByName("root") == null){
        addUser("root", "toor", true);
//        }else {
//            getUserByName("root").setPassword("toor");
        }
//        addUser("admin", "admin", true);
    }
    
    public void clear(){
        accounts = new ArrayList<>();
        saveUser();
    }
    
    public String getStatus() {
        return status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getActiveAccount() {
        return activeAccount;
    }

    public boolean isAuth() {
        return auth;
    }

    public void addUser(String name, String password, boolean admin){
        if (getUserByName(name) == null && !name.contains("&")) {
            accounts.add(new User(name, password, admin));
        }
        saveUser();
    }
    
    public void addUser() {
        if (getUserByName(name) == null && !"".equals(name) && !name.contains("&")) {
            accounts.add(new User(name, password));
        }
        saveUser();
    }

    public ArrayList<User> getAccounts() {
        return accounts;
    }

    public static void delUser(User user) {
        accounts.remove(user);
        
    }

    public static String hash(String password) {
        try {
            MessageDigest gluton = MessageDigest.getInstance("SHA-256");
            byte[] dig = gluton.digest(password.getBytes(StandardCharsets.UTF_8));
            byte[] baseEncoded = Base64.getUrlEncoder().withoutPadding().encode(dig);
            String secret = new String(baseEncoded, StandardCharsets.UTF_8);
            return secret;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Authentificator.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void authenticate(String name, String password) {
        User user = getUserByName(name);
        if (user == null) {
            status = "Login fehlgeschlagen";
            return;
        }
        if (user.getSecret().equals(Authentificator.hash(password))) {
            auth = true;
            activeAccount = user;
            status = "Login erfolgreich";
            welcome = "Willkommen mein Herr und Meister";
            this.name = "";
        } else {
            status = "Passwort nicht erkannt";
        }

    }

    public void authenticate() {
        authenticate(name, password);
    }

    public void logOff() {
        if(activeAccount.isAdmin()){
            saveUser();
        }
        auth = false;
        activeAccount = null;
        welcome = "Wilkommen, bitte authentifizieren Sie sich:";
        status = "Nicht eingeloggt";
        name = "";

    }

    public static User getUserByName(String name) {
        for (User luser : accounts) {
            if (luser.getName().equals(name)) {
                return luser;
            }
        }
        return null;
    }
    public void saveUser(){
        try {
            Persistence.saveUserToFile(path, accounts);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Authentificator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loadUser(){
        try {
           accounts =  Persistence.loadUserFromFile(path);
        } catch (IOException ex) {
            Logger.getLogger(Authentificator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

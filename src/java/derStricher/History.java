/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derStricher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author valentinpaal
 */
@ApplicationScoped
public class History implements Serializable {

    private ArrayList<Monat> history = new ArrayList<>();

    public History() {
        createFakeHistory();
        loadHistory();
    }

    public void loadHistory() {
        history = new ArrayList<>();
        int monat = getMonat();
        int year = getYear();
        while (true) {
            try {
                if (monat <= 1) {
                    monat = 12;
                    year--;
                } else {
                    monat--;
                }
                
                Monat monatObj = new Monat(monat + "-" + year, Persistence.loadAzubiFromFile(monat + "-" + year));
                history.add(monatObj);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(History.class.getName()).log(Level.SEVERE, null, ex);
                break;
            } catch (IOException ex) {
                Logger.getLogger(History.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void loadByDate(int month, int year) throws Exception {
        Persistence.loadAzubiFromFile(month + "-" + year);
    }

    public ArrayList<Monat> getHistory() {
        return history;
    }

    public void setHistory(ArrayList<Monat> history) {
        this.history = history;
    }

    public void addMonat(Monat monat) {
        history.add(monat);
    }

    public int getMonat() {
        return Calendar.getInstance().get(Calendar.MONTH);
    }

    public int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public void createFakeHistory() {
        int monat = 12;
        int year = getYear() - 1;

        for (int i = 0; i < 10; i++) {
            ArrayList<Azubi> azubis = new ArrayList<>();
            azubis.add(new Azubi("Leo", 2 * i, 20 - i));
            azubis.add(new Azubi("Don", 3 - i, i));
            azubis.add(new Azubi("Kai", 2 * i, 11 - i));
            azubis.add(new Azubi("Vale", 28 - 2 * i, i));
            try {
                Persistence.saveAzubiToFile(String.valueOf(monat) + "-" + String.valueOf(year), azubis);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(History.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (monat == 1) {
                monat = 12;
                year--;
            } else {
                monat--;
            }
        }

    }

}

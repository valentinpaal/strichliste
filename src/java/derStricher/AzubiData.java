/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derStricher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author valentinpaal
 */


//TODO UI aufräumen
//TODO Admin auswahl
@ApplicationScoped
@Named("azubiData")
public class AzubiData implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Authentificator AUTHENTIFICATOR = new Authentificator();
    private static final History HISTORY = new History();

    private String name;
    private static ArrayList<Azubi> azubis;

    public AzubiData() {
        azubis = new ArrayList<>();
        load();
        
    }

    
    private void createNewList() {
        ArrayList<Azubi> newList = new ArrayList<>();
        azubis.forEach((azubi) -> {
            newList.add(new Azubi(azubi.getName()));
        });
        azubis = newList;
    }
    
    public void save() {
        try {
            Persistence.saveAzubiToFile(getDateString(), azubis);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AzubiData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     *
     */
    private void load() {
        try {
            azubis = Persistence.loadAzubiFromFile(getDateString());
        } catch (IOException ex) {
            if (ex.equals(new FileNotFoundException())){
                //neuer Monat
                createNewList();
            }
            Logger.getLogger(AzubiData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Azubi> getAzubis() {
        save();
        return azubis;
    }


    public String getDateString(){
        int monat = getMonat();
        int year = getYear();
        return String.valueOf(monat) + "-" + String.valueOf(year);
    }
    public int getMonat(){
        return Calendar.getInstance().get(Calendar.MONTH);       
    }
    
    public int getYear(){
        return Calendar.getInstance().get(Calendar.YEAR);
    }
    


    public static Authentificator getAuthentificator() {
        return AUTHENTIFICATOR;
    }

    public void addAzubi() {
        if (name != null && !"".equals(name) && !name.contains("&")) {

            if (getAzubiByName(name) == null) {
                azubis.add(new Azubi(name));
            }
            name = "";
        }
    }

    public Azubi getAzubiByName(String name) {
        for (Azubi azubi : azubis) {
            if (azubi.getName().equals(name)) {
                return azubi;
            }
        }
        return null;

    }

    public History getHistory() {
        return HISTORY;
    }

    public void deleteAzubi(String name) {
        azubis.remove(getAzubiByName(name));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
